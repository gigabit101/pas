package PeanutsAndStuff;

import java.io.File;

import net.minecraft.creativetab.CreativeTabs;
import PeanutsAndStuff.block.ModBlocks;
import PeanutsAndStuff.config.PatConfig;
import PeanutsAndStuff.creativetab.CreativeTabPAS;
import PeanutsAndStuff.item.ModItems;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

/**
 * Created by MJ on 6/11/2014.
 */

@Mod(modid = PeanutsAndStuff.ID, version = PeanutsAndStuff.Version, name = PeanutsAndStuff.NAME)
public class PeanutsAndStuff {
	
	
	public static PatConfig properties;
	public static final String ID = "Pat";
	public static final String NAME = "PeanutsAndStuff";
	public static final String Version = "2.0";

    @Mod.Instance
    public static PeanutsAndStuff instance;

    //Adds Peanuts And Stuff Creative Tab
    public static CreativeTabs tabsPAS = new CreativeTabPAS(CreativeTabs.getNextID(), "Peanuts And Stuff");

    @Mod.EventHandler
    public void preLoad(FMLPreInitializationEvent event)
    {
		instance = this;
		// This should be the FIRST thing that gets done.
		String path = event.getSuggestedConfigurationFile().getAbsolutePath()
				.replace(PeanutsAndStuff.ID, "Talismans");

		properties = PatConfig.initialize(new File(path));
    	//Loads Mod Items
        ModItems.init();
        //Loads Mod Blocks
        ModBlocks.Peanutcropinit(properties);
        
    }

    @Mod.EventHandler
    public void load(FMLInitializationEvent evt)
    {
    	//Loads Recipes
        ModItems.initIRecipes();
    }

    @Mod.EventHandler
    public void postLoad(FMLPostInitializationEvent evt)
    {

    }

}
