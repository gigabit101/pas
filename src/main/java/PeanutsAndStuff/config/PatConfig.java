package PeanutsAndStuff.config;

import java.io.File;

import net.minecraftforge.common.config.Configuration;

public class PatConfig {

	private static PatConfig instance = null;

	 private final String CATEGORY_CRAFTING = "crafting";

		public final boolean peanutCropsAllowed;


	private PatConfig(File configFile) {
		Configuration config = new Configuration(configFile);
		config.load();
		//peanut crops
		 peanutCropsAllowed = config.get(CATEGORY_CRAFTING, "Allow peanut crops", true).getBoolean(true);

		config.save();

	}

	public static PatConfig initialize(File configFile) {

		if (instance == null)
			instance = new PatConfig(configFile);
		else
			throw new IllegalStateException(
					"Cannot initialize PasConfig twice");

		return instance;
	}

	public static PatConfig instance() {
		if (instance == null) {

			throw new IllegalStateException(
					"Instance of PasConfig requested before initialization");
		}
		return instance;
	}

}
