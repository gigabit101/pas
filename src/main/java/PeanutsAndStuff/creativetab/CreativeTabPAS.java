package PeanutsAndStuff.creativetab;

import PeanutsAndStuff.item.ModItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * Created by MJ on 6/11/2014.
 */
public class CreativeTabPAS extends CreativeTabs {

    public CreativeTabPAS(int tabID, String tabLabel){
        super(tabID, tabLabel);
    }

    public String getTranslatedTabLabel()
    {
        return "Peanuts And Stuff";
    }

    @Override
    public Item getTabIconItem(){
        return ModItems.pas_Peanut;
    }
}
