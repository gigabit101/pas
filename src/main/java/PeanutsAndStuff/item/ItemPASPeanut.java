package PeanutsAndStuff.item;

import ibxm.Player;

import java.util.List;

import cpw.mods.fml.relauncher.SideOnly;
import PeanutsAndStuff.PeanutsAndStuff;
import PeanutsAndStuff.block.ModBlocks;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.event.entity.player.BonemealEvent;

/**
 * Created by MJ on 6/11/2014.
 */
public class ItemPASPeanut extends ItemFood implements IPlantable{
	
	public static final String[] types = new String[] { "pas_Peanut", "pas_CircusPeanut",
		"pas_MoldyPeanut", "pas_SuperPeanut", "pas_RoastedPeanut", "pas_BurntPeanut" };
	private static int healAmount;
	private Block blockType;
    private Block soilBlock;
	private IIcon[] textures;
	
	
	
    public ItemPASPeanut(Block blockType, Block soilBlock )
    {
        super(healAmount, 0, false);
        this.setCreativeTab(PeanutsAndStuff.tabsPAS);
		setHasSubtypes(true);
        this.setUnlocalizedName("Peanut");
        this.blockType = blockType;
        this.soilBlock = soilBlock;

    }
    
	@Override
	// Registers Textures For All Peanuts
	public void registerIcons(IIconRegister iconRegister)
    {
		textures = new IIcon[types.length];

		for (int i = 0; i < types.length; ++i) {
			textures[i] = iconRegister.registerIcon("PeanutsAndStuff:" + types[i]);
		}
	}
	// Adds Texture what match's meta data
	public IIcon getIconFromDamage(int meta)
    {
		if (meta < 0 || meta >= textures.length) {
			meta = 0;
		}

		return textures[meta];
	}

	@Override
	// gets Unlocalized Name depending on meta data
	public String getUnlocalizedName(ItemStack itemStack)
    {
		int meta = itemStack.getItemDamage();
		if (meta < 0 || meta >= types.length) {
			meta = 0;
		}

		return super.getUnlocalizedName() + "." + types[meta];
	}

	// Adds Peanuts SubItems To Creative Tab
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
    {
		for (int meta = 0; meta < types.length; ++meta) {
			list.add(new ItemStack(item, 1, meta));
		}
	}


	@Override
	public EnumPlantType getPlantType(IBlockAccess world, int x, int y, int z) {
        return (blockType == Blocks.nether_wart ? EnumPlantType.Crop : EnumPlantType.Crop);
	}

	@Override
	public Block getPlant(IBlockAccess world, int x, int y, int z) {
        return blockType;
	}

	@Override
	public int getPlantMetadata(IBlockAccess world, int x, int y, int z) {
		return 1;
	}
	
	@Override
	//Allows Peanuts to be planted 
    public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int side, float hitVecX, float hitVecY, float hitVecZ)
    {
        if (side != 1)
        {
            return false;
        }
        else if (player.canPlayerEdit(x, y, z, side, itemStack) && player.canPlayerEdit(x, y + 1, z, side, itemStack))
        {
    		Block soil = world.getBlock(x, y, z);

            if (soil != null && soil.canSustainPlant(world, x, y, z, ForgeDirection.UP, this) && world.isAirBlock(x, y + 1, z))
            {
                world.setBlock(x, y + 1, z, ModBlocks.PeanutPlant, 0, 2);
                --itemStack.stackSize;
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
	
	@Override
	//Eating
    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer player)
    {
		if (itemstack.getItemDamage() == 1)
		{
			if (player.canEat(true))
			{
				player.setItemInUse(itemstack, this.getMaxItemUseDuration(itemstack));
			}
		}
		else if (itemstack.getItemDamage() == 10)
		{
			if (player.canEat(true))
			{
				player.setItemInUse(itemstack, this.getMaxItemUseDuration(itemstack));
			}
		}
		else
		{
			if (player.canEat(false))
			{
				player.setItemInUse(itemstack, this.getMaxItemUseDuration(itemstack));
			}
		}

        return itemstack;
    }

	@Override
    public ItemStack onEaten(ItemStack itemstack, World world, EntityPlayer player)
    {
		--itemstack.stackSize;
		switch (itemstack.getItemDamage())
		{
		        //Peanut
			case 0:
				player.getFoodStats().addStats(1, 0.1F);
				break;

				//Circus Peanut
			case 1:
				player.getFoodStats().addStats(0, 0.1F);
                if (world.rand.nextFloat() < 0.6F)
                    player.addPotionEffect(new PotionEffect(Potion.confusion.id, 225, 1));
        		break;

				//Moldy peanut
			case 2:
				player.getFoodStats().addStats(3, 0.1F);
				//Poisons the player who eats it 
				if (world.rand.nextFloat() < 0.6F)
       				player.addPotionEffect(new PotionEffect(Potion.poison.id, 225, 0));
                    player.addPotionEffect(new PotionEffect(Potion.hunger.id, 225, 1));
				break;

				//super Peanut
			case 3:
				player.getFoodStats().addStats(2, 0.5F);

                if (world.rand.nextFloat() < 0.6F)
                player.addPotionEffect(new PotionEffect(Potion.blindness.id, 150, 0));
                player.addPotionEffect(new PotionEffect(Potion.digSpeed.id, 2250, 0));
                player.addPotionEffect(new PotionEffect(Potion.invisibility.id, 2250, 0));
                player.addPotionEffect(new PotionEffect(Potion.waterBreathing.id, 2250, 0));
                player.addPotionEffect(new PotionEffect(Potion.nightVision.id, 2250, 0));
                player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 2250,0));
                player.addPotionEffect(new PotionEffect(Potion.jump.id, 225, 50));
				break;

				//roasted Peanut
			case 4:
				player.getFoodStats().addStats(6, 0.6F);
				break;

				//Burnt Peanut
			case 5:
				player.getFoodStats().addStats(6, 0.6F);
				break;


			default:
				player.getFoodStats().addStats(0, 0.0F);
				break;
		}

        world.playSoundAtEntity(player, "random.burp", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
        return itemstack;
    }

    @Override
    //Adds Tooltip to Super Peanut 
    public void addInformation(ItemStack iS, EntityPlayer eP, List l, boolean b)
    {
        if(iS.getItemDamage() == 3)
        {
            l.add("Don't jump ;)");
        }
    }
	  

}  
