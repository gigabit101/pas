package PeanutsAndStuff.item;

import java.util.Collection;

import PeanutsAndStuff.block.ModBlocks;
import cpw.mods.fml.common.Mod;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;

/**
 * Created by MJ on 6/11/2014.
 */
public class ModItems {


    public static Item pas_Peanut;

    public static void init()
    {
        pas_Peanut = regI(new ItemPASPeanut(ModBlocks.PeanutPlant, Blocks.farmland).setUnlocalizedName("Peanut"));
        
        //Makes Peanuts Drop From Tall Grass
        MinecraftForge.addGrassSeed (new ItemStack (pas_Peanut), 30 );
        //Makes Peanuts Drop From Tall Grass
        MinecraftForge.addGrassSeed (new ItemStack (pas_Peanut,1,2), 10 );


    }

    public static Item regI(Item i)
    {
        GameRegistry.registerItem(i, i.getUnlocalizedName().replace("item.", ""));
        return i;
    }

    public static void initIRecipes()
    {
        //P to RP
        GameRegistry.addSmelting(new ItemStack(ModItems.pas_Peanut, 1, 0), new ItemStack(ModItems.pas_Peanut, 1, 4), 0);
        //RP to BP
        GameRegistry.addSmelting(new ItemStack(ModItems.pas_Peanut, 1, 4), new ItemStack(ModItems.pas_Peanut, 1, 5), 1);
        //BP to C
        GameRegistry.addSmelting(new ItemStack(ModItems.pas_Peanut, 1, 5), new ItemStack(Items.coal, 1, 1), 0);
        //RP to SP
        GameRegistry.addShapedRecipe(new ItemStack(ModItems.pas_Peanut, 1, 3), new Object[]{
                "XZX", "XCX", "XZX", 'X', Blocks.beacon ,'Z', Blocks.emerald_block, 'C', new ItemStack(ModItems.pas_Peanut, 1, 4)
        });

    }
}