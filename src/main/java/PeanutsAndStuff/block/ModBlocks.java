package PeanutsAndStuff.block;

import PeanutsAndStuff.config.PatConfig;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.Item;

/**
 * Created by MJ on 6/11/2014.
 */
public class ModBlocks {

	public static Block PeanutPlant;
	
    public static void Peanutcropinit(PatConfig properties)
    {
    	if(properties.peanutCropsAllowed)
    	PeanutPlant =  new BlockPeanutPlant();
		GameRegistry.registerBlock(PeanutPlant, "PeanutPlant");

    }

}
