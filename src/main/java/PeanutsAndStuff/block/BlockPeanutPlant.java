package PeanutsAndStuff.block;

import java.util.Random;

import javax.xml.ws.FaultAction;

import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import PeanutsAndStuff.PeanutsAndStuff;
import PeanutsAndStuff.item.ModItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.event.entity.player.BonemealEvent;

public class BlockPeanutPlant extends BlockCrops {

	@SideOnly(Side.CLIENT)
	private IIcon[] iconArray;

	protected BlockPeanutPlant() {
		super();
		this.setBlockName("PeanutCrop");
		setTickRandomly(true);
		this.setBlockTextureName("peanutplant");

	}

	@Override
	//Gets Icon From Meta Data
	public IIcon getIcon(int side, int meta) {
		if (meta < 7) {
			if (meta == 6) {
				meta = 5;
			}

			return this.iconArray[meta >> 1];
		} else {
			return this.iconArray[3];
		}
	}

	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x,
			int y, int z) {
		return null;
	}

	public int getRenderType() {
		return 6;
	}

	public boolean isOpaqueCube() {
		return false;

	}

	@Override
	// TODO: getSeedItem()
	protected Item func_149866_i() {
		return ModItems.pas_Peanut;
	}

	@Override
	// TODO: getCropItem()
	protected Item func_149865_P() {
		return ModItems.pas_Peanut;
	}

	@Override
	public void registerBlockIcons(IIconRegister iconRegister) {
		this.iconArray = new IIcon[4];

		for (int i = 0; i < this.iconArray.length; ++i) {
			this.iconArray[i] = iconRegister.registerIcon("peanutsandstuff:"
					+ this.getTextureName() + "_stage_" + i);
		}
		
	}

  }
